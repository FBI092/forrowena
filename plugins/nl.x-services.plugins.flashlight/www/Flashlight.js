function Flashlight() {
  // track flashlight state
    console.log('hello Flashlight');
  this._isSwitchedOn = false;
    console.log(this._isSwitchedOn);
}
Flashlight.prototype = {

  available: function (callback) {
      console.log('hello Flashlight available');
    cordova.exec(function (avail) {
      callback(avail ? true : false);
    }, null, "Flashlight", "available", []);
  },

  switchOn: function (successCallback, errorCallback) {
      console.log('hello Flashlight switchOn');
    this._isSwitchedOn = true;
    cordova.exec(successCallback, errorCallback, "Flashlight", "switchOn", []);
  },

  switchOff: function (successCallback, errorCallback) {
      console.log('hello Flashlight switchOff');
    this._isSwitchedOn = false;
    cordova.exec(successCallback, errorCallback, "Flashlight", "switchOff", []);
  },

  toggle: function (successCallback, errorCallback) {
      console.log('hello Flashlight toggle');
    if (this._isSwitchedOn) {
      this.switchOff(successCallback, errorCallback);
    } else {
      this.switchOn(successCallback, errorCallback);
    }
  }
};

Flashlight.install = function () {
  if (!window.plugins) {
    window.plugins = {};
  }

  window.plugins.flashlight = new Flashlight();
  return window.plugins.flashlight;
};

cordova.addConstructor(Flashlight.install);