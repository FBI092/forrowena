# README #
# Тех Задание #
use these graphics - http://graphicriver.net/item/health-kit-app/8168652?WT.oss_phrase=health%20app&WT.oss_rank=1&WT.z_author=Zenimot&WT.ac=search_list

**Background/concept:**

It is to aid weight loss / promote a more active lifestyle. There is research to show that in the modern sedentary lifestyle where many sit at a computer / desk all day, we are not moving as much as we were designed to which is leading to increased obesity and the consequences of that. The concept of this app is to aid and increase movement throughout the day, using a principle encouraged by weight loss programmes. The idea is simple: every 20 minutes of sitting/being sedentary, you move for 2 minutes.

**Description:**

I need essentially a countdown timer that times in one mode (sedentary) for 20 minutes, and then changes to a new mode (active) to time for 2 minutes. I think this should be reflected by the colour of the screen e.g. blue for 20 minutes, changing to red (pulsing) for 2 minutes. During active mode, there should be options that the user can set when turning on the app, to alert them that they are in active mode - selected when setting up the app and changed in options on the app homescreen:

1. short beep sounds repeatedly for the 2 mins e.g. every 3s
2. intermittent vibration for 2 mins
3. flashing of the camera flash for 2 mins
4. chosen music track from users music - plays the first 2 mins only
5. music - a track on the app - plays for 2 mins

1, 4 and 5 don't sound when the phone is in silent mode - and when selecting any of these options, user is reminded to turn phone off silent for them to work.

There is a 'sleep' mode button displayed on the app running screen which when touched changes in to a cartoon face - simple, just two dots for eyes and a line/dot with zzz animation ie. the personification of being asleep - which pauses theapp until the 'awake' mode button is touched, at which point the app continues from a new 20/2 min cycle.

The app needs to be designed to run in the background as most users will use it throughout the day alongside their daily life e.g. email, browser etc. This is how it is envisaged that most users would use the app.

**Advanced features for second release:**

In a more advanced release of the app, it would time 20 mins (or countdown 20 minutes) in e.g. green (no pulsing) and then pend in a red pulsing until it feels a movement of the device (continuous movement for 15 s required to switch to active mode) when it switches to pulsing (different speed pulsing) blue for 2 minutes (active mode). Additionally, users will be able to share their progress to facebook/twitter e.g. "I completed my #20/2active goal today" - which would be offered to to user after they complete a specified amount of hours using the app continuously, successfully initiating the active phase within 15s of indication to the user to move and completing moving for the 2 mins when the app has indicated to move. Users would specify the amount of hours for their goal in the options of the app - and would be asked to insert this goal when setting up the app initially.

**Different screens are:** 

Home
Options/Settings
Sleep mode
Active mode
Sedentary mode
Pending Active
Completed Goal/Share screen

**User journey as follows**

Install app
Open app
Set up options - select active mode alert type from options of my music, app music, beep, vibrate or flash, select daily goal (ie. consecutive 20/2 cycles to complete today) from how many hours to run the app e.g. 8 hours (24 cycles) (ie. a full working day) number range would be 1 hour (3 cycles) to 24 hours (72 cycles). After initial set up, can select last used settings (which are displayed and user confirms them).
Start button, first cycle begins in sedentary mode, then after 20 mins moves to active mode (in advanced release this goes to pending active mode first then active once user moves device for 15s continuously ie. walking)
App continues cycling until user presses 'Sleep' (or closes the app completely - ie. still runs in the background unless the app is turned off specifically) to enter Sleep mode. Stays here until user presses "Awake" where it re-enters the cycles starting from sedentary mode again.
In advanced release, once goal number of cycles / hours are completed, app vibrates in a different sequence to alert user and share screen comes up with options to share pre-determined text (editable) to linked twitter / facebook accounts. Choice to skip or share. After this, goes back to home screen.

То что ниже позже изменю
### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact