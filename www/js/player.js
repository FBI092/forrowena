var Player = {
    media :null,
    isPlaying: false,
    loop: true,
    initMedia: function(path) {
        if(device.platform === 'iOS') {
            path = path.replace('file://', '');
        }
        Player.media = new Media(
            path,
            function() {
                console.log('Media file read succesfully');
                if (Player.media !== null && Player.loop !== true){
                    Player.media.release();
                }
            },
            function(error) {
                //navigator.notification.alert('Unable to read the media file.', function(){}, 'Error');
                console.log('Unable to read the media file (Code): ' + error.code);
                var msg = '';
                switch (error.code) {
                    case MediaError.MEDIA_ERR_ABORTED:
                        msg = 'MEDIA_ERR_ABORTED';
                        break;
                    case MediaError.MEDIA_ERR_NETWORK:
                        msg = 'MEDIA_ERR_NETWORK';
                        break;
                    case MediaError.MEDIA_ERR_DECODE:
                        msg = 'MEDIA_ERR_DECODE';
                        break;
                    case MediaError.MEDIA_ERR_NONE_SUPPORTED:
                        msg = 'MEDIA_ERR_NONE_SUPPORTED';
                        break;
                    default:
                        msg = 'Unknown Error (Code):' + error.code;
                        break;
                }
                Application.alert('Player Error!!!','Error: ' + msg);
            },
            function (status){
                // повтор аудио файла
                if (status === Media.MEDIA_STOPPED){
                    if(Player.loop === true) {
                        console.log('loop audio');
                        Player.media.play();
                    }
                }
            }
        );
    },
    playPause: function(path) {
        if (Player.media === null){
            Player.initMedia(path);
        }
        if (Player.isPlaying === false){
            Player.media.play();
        }
        else {
            Player.media.pause();
        }
        Player.isPlaying = !Player.isPlaying;
    },
    stop: function(){
        if (Player.media !== null){
            Player.media.stop();
            Player.media.release();
        }
        Player.media = null;
        Player.isPlaying = false;
    }
};