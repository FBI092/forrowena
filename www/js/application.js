/**
 * Created by Dell-STO on 11.03.2015.
 */
var Application = {
    timer : null,
    src: null,
    directoryPath: null,
    srcarray: [],
    data: 'test',
    initFilesListPage: function(){
        $(".class_Waiting-popup").popup("open");
        setTimeout(function () {
            Application.updateMediaList();
        }, 150);
        $(document).on('endupdate', function () {
            $('.class_Waiting-popup').popup('close');
            Application.createFilesList('files-list', AppFile.getAppFiles());
        });
        Application.createFilesList('files-list', AppFile.getAppFiles());
    },
    exit: function(){
        $('<div>').simpledialog2({
            mode: 'button',
            themeDialog: SaveData.returnThemeID(),
            themeHeader: SaveData.returnThemeID(),
            headerText: 'Exit',
            buttons : {
                'OK': {
                    click: function () {
                        console.log('Ok');
                        CountDown.stop();
                        navigator.app.exitApp();
                    }
                },
                'Cancel': {
                    click: function () {
                        console.log('Cancel');
                    },
                    icon: "delete"
                }
            }
        });
    },
    updateMediaList: function(){
        console.log('updateMediaList');
        window.requestFileSystem(
            LocalFileSystem.PERSISTENT,
            0,
            function(fileSystem){
                var root = fileSystem.root;
                Application.directoryPath = root.nativeURL;
                console.log(root.nativeURL);
                Application.collectMedia(root, true);
            },
            function(error){
                var msg = '';
                switch (error.code) {
                    case FileError.QUOTA_EXCEEDED_ERR:
                        msg = 'QUOTA_EXCEEDED_ERR';
                        break;
                    case FileError.NOT_FOUND_ERR:
                        msg = 'NOT_FOUND_ERR';
                        break;
                    case FileError.SECURITY_ERR:
                        msg = 'SECURITY_ERR';
                        break;
                    case FileError.INVALID_MODIFICATION_ERR:
                        msg = 'INVALID_MODIFICATION_ERR';
                        break;
                    case FileError.INVALID_STATE_ERR:
                        msg = 'INVALID_STATE_ERR';
                        break;
                    default:
                        msg = 'Unknown Error';
                        break;
                }
                Application.alert('File System!!!','Error: ' + msg);
            },
            false);
    },
    // Поиск записей на устройстве
    collectMedia: function(path, recursive, level){
        if(level === undefined){
            level = 0;
        }
        var directoryEntry = new DirectoryEntry('', path.fullPath);
        if(!directoryEntry.isDirectory){
            Application.alert('Directory path!!!' ,'The provided path is not directory');
            return;
        }

        path.createReader().readEntries(function(entries){
                var appFile;
                var extension;
            for (var i = 0; i < entries.length; i++){
                if (entries[i].name === '.'){
                    continue;
                }
                extension = entries[i].name.substr(entries[i].name.lastIndexOf('.'));
                if (entries[i].isDirectory === true && recursive === true){
                    setTimeout(Application.collectMedia(entries[i], recursive, level +1),150);
                }else if (entries[i].isFile === true && $.inArray(extension, AppFile.EXTENSIONS) >= 0){
                    if(SaveData.returnDevice() === 'iOS'){
                        var test = entries[i].nativeURL;
                        test = test.replace(Application.directoryPath, '');
                        appFile = new AppFile(entries[i].name, test);
                    }else {
                        appFile = new AppFile(entries[i].name, entries[i].nativeURL);
                    }
                    appFile.addFile();
                        console.log('File saved: '+ entries[i].nativeURL);
                    }
                }
        },
        function (error){
            Application.alert('Directory Read!!!','Unable to read the directory. Error code: ' +error.code);
        });
        if (level === 0){
            $(document).trigger('endupdate');
        }
        console.log('Current path analized is ' + path.nativeURL);
    },
    // Создание списка записей test
    createFilesList: function(idElement, files){
        $('#'+ idElement).empty();
        if (files == null || files.length == 0){
            $('#'+ idElement).append('<p align="center">No files to show.</p>');
            return;
        }
        // Действия при выборе трека getPlayHandler проигрывает 10 секунд выбраного трека
        function getPlayHandler(file){
            return function playHandler(){
                if(CountDown.isActive === false) {
                    clearTimeout(Application.timer);
                    Player.stop();
                    Player.loop = false;
                    setTimeout(function () {
                        if (SaveData.returnDevice() === 'iOS') {
                            var path = Application.directoryPath + file.audioPath;
                            path = path.replace("file://localhost/", "file:///");
                            path = decodeURI(path);
                            Player.playPause(path);
                        }else {
                            Player.playPause(file.audioPath);
                        }
                        Player.loop = true;
                        console.log('Play');
                        Application.timer = setTimeout(function () {
                            Player.stop();
                            Player.loop = false;
                            console.log('Stop');
                        }, 10000);
                    }, 150)
                }
                else {
                    Application.alert('Wait!!!' ,'App in Active mode!!! Please wait while the active mode will end.');
                }
            };
        }
        // Сохранение выбраного трека в локальном хранилище
        function getApplyHandler(file){
            return function applyHandler(){
                clearTimeout(Application.timer);
                if(CountDown.isActive === false) {
                    Player.stop();
                    Player.loop = false;
                }
                window.localStorage.setItem("selectedAudio", JSON.stringify(file));
                $('#musicListButton').collapsible("collapse");
                $('#musicListButton .ui-collapsible-heading-toggle').text(file.audioName);
                submitSlide();
            };
        }
        function getDeleteHandler(file){
            return function deleteHandler(){
                $(".class_Delete_song").popup("open");
                jQuery('#submitDelete').unbind('click');
                jQuery('#submitDelete').click(function(){
                    if(CountDown.isActive === false) {
                        clearTimeout(Application.timer);
                        Player.stop();
                        Player.loop = false;
                        var audio = SaveData.returnAudioForPlay();
                        if((audio == '')||(audio === '')||(audio == null)||(audio === null)){
                            deleteFile();
                        }else {
                            if (((audio.audioName == file.audioName) && (audio.audioPath == file.audioPath))) {
                                var signalType = SaveData.returnSignalType();
                                $(".class_Delete_song").popup("close");
                                if (signalType == '4') {
                                    if(CountDown.status == 'Awake') {
                                        $('<div>').simpledialog2({
                                            mode: 'button',
                                            themeDialog: SaveData.returnThemeID(),
                                            themeHeader: SaveData.returnThemeID(),
                                            headerText: 'Delete song!',
                                            buttonPrompt: '<p>This file used for active option. Are you sure you want to delete this file?</p>',
                                            buttons: {
                                                'Yes': {
                                                    click: function () {
                                                        deleteFile();
                                                    }
                                                },
                                                'No': {
                                                    click: function () {
                                                    },
                                                    icon: "delete"
                                                }
                                            }
                                        });
                                    }else {
                                        $('<div>').simpledialog2({
                                            mode: 'button',
                                            themeDialog: SaveData.returnThemeID(),
                                            themeHeader: SaveData.returnThemeID(),
                                            headerText: 'Delete song!',
                                            buttonPrompt: '<p>You can not delete this song. To remove it, select another song or go to the sleep mode!</p>',
                                            buttons: {
                                                'Ok': {
                                                    click: function () {
                                                    }
                                                }
                                            }
                                        });
                                    }
                                } else {
                                    window.localStorage.removeItem("selectedAudio");
                                    window.localStorage.removeItem("audioForPlay");
                                    deleteFile();
                                }

                            } else {
                                deleteFile();
                            }
                        }
                        function deleteFile() {
                            var settingsValue = new SettingsVal('none1', 'none2', 'none3', JSON.stringify(file));
                            setTimeout(function () {
                                var path = Application.directoryPath + file.audioPath;
                                path = path.replace("file://localhost/", "");
                                path = decodeURI(path);
                                path = path.replace("file://", "");
                                window.plugins.iOSAudioPicker.deleteSongs(delSuccess, delError, 'false', path);
                                function delSuccess(a) {
                                    console.log(JSON.stringify(a));
                                }
                                function delError(e) {
                                    console.log(JSON.stringify(e));
                                }
                                var forDel = new AppFile(file.audioName, file.audioPath);
                                forDel.deleteFile();
                                settingsValue.deleteSettings();
                                var prom = SettingsVal.getAppSettingsList();
                                if((audio == '')||(audio === '')||(audio == null)||(audio === null)) {
                                }else{
                                    if (((audio.audioName == file.audioName) && (audio.audioPath == file.audioPath))) {
                                        if (prom.length >= 1) {
                                            var lastSetting = prom[prom.length - 1];
                                            SaveData._db.removeItem("cyclesCount");
                                            SaveData._db.removeItem("countdownCycles");
                                            SaveData._db.removeItem("signalType");
                                            SaveData._db.removeItem("audioForPlay");
                                            SaveData._db.removeItem("selectedAudio");
                                            SaveData._db.setItem("cyclesCount", lastSetting.cycles);
                                            SaveData._db.setItem("countdownCycles", lastSetting.cycles);
                                            SaveData._db.setItem("signalType", lastSetting.signalType);
                                            if (lastSetting.audio !== "none") {
                                                SaveData._db.setItem("audioForPlay", lastSetting.audio);
                                            }
                                        } else {
                                            SaveData._db.removeItem("cyclesCount");
                                            SaveData._db.removeItem("countdownCycles");
                                            SaveData._db.removeItem("signalType");
                                            SaveData._db.removeItem("audioForPlay");
                                            SaveData._db.removeItem("selectedAudio");
                                        }
                                    }
                                }
                                OptionList.displayOptionList();
                                Application.displayAudioList();
                                if((audio == '')||(audio === '')||(audio == null)||(audio === null)) {
                                }else{
                                    if(SaveData.returnSignalType() == '4'){
                                        SaveData.setData();
                                    }
                                }
                                $(".class_Delete_song").popup("close");
                            }, 150)
                        }
                    }
                });
            };
        }
        var $listElement, $linkElement, $iconsElsement;
        files.sort(AppFile.compareIgnoreCase);
        for(var i = 0; i < files.length; i++){
            $listElement = $('<li>');
            $linkElement = $('<a>');
            $linkElement
                .attr('href', '#')
                .text(files[i].audioName)
                .click(getPlayHandler(files[i]));

            $listElement.append($linkElement);

            if(SaveData.returnDevice() === 'iOS'){
                $iconsElsement = $('<div>');
                $iconsElsement
                    .attr('class', 'split-custom-wrapper');

                $linkElement = $('<a>');
                $linkElement
                    .attr('href', '#')
                    .attr('class','split-custom-button ui-link ui-btn ui-icon-delete ui-btn-icon-notext ui-shadow ui-corner-all')
                    .attr('data-rel','dialog')
                    .click(getDeleteHandler(files[i]));

                $iconsElsement.append($linkElement);
                $linkElement = $('<a class="split-custom-button">');
                $linkElement
                    .attr('href', '#')
                    .attr('class','split-custom-button ui-link ui-btn ui-icon-check ui-btn-icon-notext ui-shadow ui-corner-all')
                    .attr('data-rel','dialog')
                    .click(getApplyHandler(files[i]));

                $iconsElsement.append($linkElement);
                $listElement.append($iconsElsement);
            }else{
                $linkElement = $('<a>');
                $linkElement
                    .attr('href', '#')
                    .attr('data-icon', 'check')
                    .text('Check')
                    .click(getApplyHandler(files[i]));

                $listElement.append($linkElement);
            }

            $('#' + idElement).append($listElement);
        }
        $('#' + idElement).listview('refresh');
    },
    displayAudioList: function() {
        var audio = SaveData.returnAudioForPlay();
        if (audio !== null) {
            $('#musicListButton .ui-collapsible-heading-toggle').text(audio.audioName);
        }else{
            $('#musicListButton .ui-collapsible-heading-toggle').text("Choose a music composition...");
        }
        Application.createFilesList('files-list', AppFile.getAppFiles());
    },
    alert: function (title, text){
        navigator.notification.alert(text, function(){}, title);
    }
};