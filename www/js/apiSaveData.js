var lastSettingsList = [];
var SaveData = {
    _db : window.localStorage,
    isOnn : false,
    startStop : null,
    setVal: function(obj) {
        var field = obj.id;
        var h, m;
        var slider = document.getElementById('slider');
        var number = document.getElementById('number');
        var hours = document.getElementById('hours');
        var minutes = document.getElementById('minutes');
        switch (field) {
            case "slider":
                m = obj.value % 3;
                h = (obj.value - m) / 3;
                hours.value = h;
                if(m == 0){
                    minutes.value = "00";
                }else {
                    minutes.value = m * 20;
                }
                number.value = obj.value;
                break;
            case "number":
                slider.value = obj.value;
                $('#slider').slider('refresh');
                m = obj.value % 3;
                h = (obj.value - m) / 3;
                hours.value = h;
                if(m == 0){
                    minutes.value = "00";
                }else {
                    minutes.value = m * 20;
                }
                break;
            case "hours":
            case "minutes":
                if(hours.value >= 24){
                    minutes.value = "00";
                    hours.value = 24;
                }
                if((minutes.value > 40)&&(hours.value < 24)) {
                    minutes.value = "00";
                    hours.value = +hours.value + 1;
                }else{
                    minutes.value = "00";
                }
                hours = +hours.value * 3;
                minutes = +minutes.value / 20;
                var test = hours + minutes;
                slider.value = test;
                $('#slider').slider('refresh');
                number.value = test;
                break;
            default:
                number.value = obj;
                slider.value = obj;
                $('#slider').slider('refresh');
                m = obj % 3;
                h = (obj - m) / 3;
                hours.value = h;
                if(m == 0){
                    minutes.value = "00";
                }else {
                    minutes.value = m * 20;
                }
        }
    },
    // Сохранение данных в локальном хранилище
    saveData: function(){
        var signalType = document.getElementById('signalType').value;
        var sliderVal = document.getElementById("slider").value;
        var date = new Date().toLocaleString();
        var settingsValue;
        function submitData(signal){
            switch (signal) {
                case '4':
                    var file = JSON.parse(_db.getItem("selectedAudio"));
                    var file2 = SaveData.returnAudioForPlay();
                    if (((file == '') || (file === '') || (file == null) || (file === null)) &&
                        ((file2 == '') || (file2 === '') || (file2 == null) || (file2 === null))) {
                        Application.alert('Warning!!!', 'You don\'t select music composition');
                    }
                    else if ((file == '') || (file === '') || (file == null) || (file === null)) {
                        SaveData._db.setItem("signalType", signal);
                        settingsValue = new SettingsVal(date, signal, sliderVal, SaveData.returnAudioForPlay());
                        settingsValue.addSettings();
                        location.replace("#settings");
                        OptionList.displayOptionList();
                        console.log('Data was Saved');
                        $("#submitField").slideUp();
                        $("#startButton").slideDown();
                    }
                    else {
                        SaveData._db.setItem("signalType", signal);
                        SaveData._db.setItem("audioForPlay", JSON.stringify(file));
                        settingsValue = new SettingsVal(date, signal, sliderVal, JSON.stringify(file));
                        settingsValue.addSettings();
                        location.replace("#settings");
                        OptionList.displayOptionList();
                        console.log('Data was Saved');
                        $("#submitField").slideUp();
                        $("#startButton").slideDown();
                    }
                    break;
                case '3':
                    window.plugins.flashlight.available(function (isAvailable) {
                        console.log(isAvailable);
                        if (isAvailable) {
                            if(device.platform === 'iOS') {
                                Application.alert('Flashlight!!!', 'Flashlight doesn\'t work when Application in background!');
                            }
                            SaveData._db.setItem("signalType", signal);
                            settingsValue = new SettingsVal(date, signal, sliderVal, "none");
                            settingsValue.addSettings();
                            location.replace("#settings");
                            OptionList.displayOptionList();
                            console.log('Data was Saved');
                            $("#submitField").slideUp()
                            $("#startButton").slideDown();
                        } else {
                            console.log('Flashlight not available on this device');
                            Application.alert('Flashlight!!!', 'This option not available on this device!!! Please select another option.');
                        }
                    });
                    break;
                case '1':
                case '2':
                case '5':
                default :
                    SaveData._db.setItem("signalType", signal);
                    settingsValue = new SettingsVal(date, signal, sliderVal, "none");
                    settingsValue.addSettings();
                    location.replace("#settings");
                    OptionList.displayOptionList();
                    console.log('Data was Saved');
                    $("#submitField").slideUp();
                    $("#startButton").slideDown();
            }
            if (sliderVal !== SaveData.returnSavedCycles()) {
                SaveData._db.setItem("cyclesCount", sliderVal);
                SaveData._db.setItem("countdownCycles", sliderVal);
            }
            else if ((SaveData.returnCountdownCycles() <= 0) || (SaveData.returnCountdownCycles() === null)) {
                SaveData._db.setItem("countdownCycles", sliderVal);
            }
            SaveData._db.setItem("device", device.platform);
        }
        if (CountDown.isActive === false) {
            if(sliderVal == SaveData.returnSavedCycles()||((SaveData.returnSavedCycles() == '') ||
                (SaveData.returnSavedCycles() === '') || (SaveData.returnSavedCycles() == null) ||
                (SaveData.returnSavedCycles() === null))) {
                submitData(signalType);
            }else {
                if (CountDown.status == 'Sleep') {
                    Application.alert('Aborted','You can\'t change cycle value when application not in Sleep mode!!!');
                } else {
                    if (SaveData.returnCountdownCycles() == SaveData.returnSavedCycles()) {
                        submitData(signalType);
                    } else {
                        $('<div>').simpledialog2({
                            mode: 'button',
                            themeDialog: SaveData.returnThemeID(),
                            themeHeader: SaveData.returnThemeID(),
                            headerText: 'Notification!',
                            buttonPrompt: '<p>The number of cycles was changed, countdown will be started from the beginning. Are you sure?</p>',
                            buttons: {
                                'Yes': {
                                    click: function () {
                                        submitData(signalType);
                                    }
                                },
                                'No': {
                                    click: function () {
                                        var cycles = SaveData.returnSavedCycles();
                                        if (cycles !== null) {
                                            SaveData.setVal(cycles);
                                        } else {
                                            SaveData.setVal(24);
                                        }
                                    },
                                    icon: "delete"
                                }
                            }
                        });
                    }
                }
            }
        }else{
            Application.alert('Aborted!!!','You can\'t change settings when application in Active mode!!!');
        }
    },
    // Установка значений из Локального хранилища
    setData: function(){
        $( "#musicListButton" ).slideUp();
        $("#addFiles").slideUp();
        var release = SaveData._db.getItem("selectedRelease");
        if((release === 'basic')||((release == '')||(release === '')||(release == null)||(release === null))){
            if((release == '')||(release === '')||(release == null)||(release === null)){
                release = 'basic';
                SaveData._db.setItem('selectedRelease', release);
            }
            $("#"+release).attr("checked",true).checkboxradio("refresh");
            $("#notification").slideDown();
        }else{
            $("#"+release).attr("checked",true).checkboxradio("refresh");
            $("#notification").slideUp();
        }
        SaveData._db.removeItem("selectedAudio");
        $('#musicListButton .ui-collapsible-heading-toggle').text("Choose a music composition...");
        var cycles = SaveData.returnSavedCycles();
        var noticeActive = document.getElementById('flip_1');
        noticeActive.value = SaveData.returnNoticeActive();
        $('#flip_1').slider('refresh');
        if(cycles !== null) {
            SaveData.setVal(cycles);
        }else{
            SaveData.setVal(24);
        }
        var signalType = document.getElementById('signalType');
        var signalTypeVal = SaveData.returnSignalType();
        if((signalTypeVal == '')||(signalTypeVal === '')||(signalTypeVal == null)||(signalTypeVal === null)) {
            console.log('This App is started for the first time!!!');
            signalType.value = "1";
            $('#signalType').selectmenu('refresh');
            $( "#musicListButton" ).slideUp();
            $("#addFiles").slideUp();
            $( "#submitField" ).slideDown();
            $( "#startButton").slideUp();
        }else{
            signalType.value = signalTypeVal;
            $('#signalType').selectmenu('refresh');
            if (signalTypeVal == "4") {
                Application.displayAudioList();
                if(SaveData.returnDevice() === 'iOS'){
                    $( "#musicListButton" ).slideDown();
                    $("#addFiles").slideDown();
                }else{
                    $( "#musicListButton" ).slideDown();
                    $("#addFiles").hide();
                }
            }
            else {
                $( "#musicListButton" ).slideUp();
                $("#addFiles").hide();
            }
            $( "#submitField" ).slideUp();
            $( "#startButton").slideDown();
        }
        console.log('Data set from local storage!');
    },
    returnNoticeActive: function(){
        return SaveData._db.getItem("noticeActive");
    },
    // Возращение количества повторов Активного режыма установленых пользователем
    returnSavedCycles: function(){
        return SaveData._db.getItem("cyclesCount");
    },
    // Возращение количества оставшыхся повторов Активного режыма
    returnCountdownCycles: function(){
        return SaveData._db.getItem("countdownCycles");
    },
    // Возращение типа сигнала
    returnSignalType: function(){
        return SaveData._db.getItem("signalType");
    },
    returnThemeID: function(){
        return SaveData._db.getItem("themeID");
    },
    returnDevice: function(){
        return SaveData._db.getItem("device");
    },
    // Возращение выбраного Пользователем трека
    returnAudioForPlay: function(){
        return JSON.parse(SaveData._db.getItem("audioForPlay"));
    }
};
function SettingsVal(date, signalType, cycles, audio){
    this.date = date;
    this.signalType = signalType;
    this.cycles = cycles;
    this.audio = audio;
    this.save = function(lastSettingsList){
        return window.localStorage.setItem("lastSettings", JSON.stringify(lastSettingsList));
    };
    this.load =function(){
        return JSON.parse(window.localStorage.getItem("lastSettings"));
    };
}
SettingsVal.save =function(lastSettingsList){
    return window.localStorage.setItem("lastSettings", JSON.stringify(lastSettingsList));
};
SettingsVal.load =function(){
    return JSON.parse(window.localStorage.getItem("lastSettings"));
};
SettingsVal.prototype.addSettings = function(){
    var index = SettingsVal.getIndex(this);
    lastSettingsList = SettingsVal.getAppSettingsList();
    if(index === false){
        lastSettingsList.push(this);
    }else{
        lastSettingsList[index] = this;
    }
    this.save(lastSettingsList);
};
SettingsVal.getAppSettingsList = function(){
    lastSettingsList = SettingsVal.load();
    if((lastSettingsList == '')||(lastSettingsList === '')||(lastSettingsList == null)||(lastSettingsList === null)){
        return [];
    }else{
        return lastSettingsList;
    }
};
SettingsVal.getIndex = function(settingsValue){
    lastSettingsList = SettingsVal.getAppSettingsList();
    for(var i = 0; i < lastSettingsList.length; i++){
        if((lastSettingsList[i].signalType === settingsValue.signalType)&&
            (lastSettingsList[i].cycles === settingsValue.cycles)&&
            (lastSettingsList[i].audio.toUpperCase() === settingsValue.audio.toUpperCase())){
            return i;
        }
    }
    return false;
};
SettingsVal.getIndexByAudio = function(audio){
    lastSettingsList = SettingsVal.getAppSettingsList();
    var list = [];
    for(var i = 0; i < lastSettingsList.length; i++){
        if(lastSettingsList[i].audio.toUpperCase() === audio.toUpperCase()){
            list.push(lastSettingsList[i]);
        }
    }
    return list;
};
SettingsVal.prototype.deleteSettings =function(){
    var index = SettingsVal.getIndexByAudio(this.audio);
    if(index.length >= 1){
        for(var i = 0; i < index.length; i++) {
            var forDel =  new SettingsVal('none', index[i].signalType, index[i].cycles, index[i].audio);
            forDel.deleteSetting();
        }
    }
    return lastSettingsList;
};
SettingsVal.prototype.deleteSetting =function(){
    var index = SettingsVal.getIndex(this);
    lastSettingsList = SettingsVal.getAppSettingsList();
    if(index !== false){
        lastSettingsList.splice(index, 1);
        this.save(lastSettingsList);
    }
    return lastSettingsList;
};
SettingsVal.getAppSetting = function(settingsValue){
    var index = SettingsVal.getIndex(settingsValue);
    console.log(index);
    if(index === false){
        console.log('getAppSettingsList='+1);
        return null;
    }else{
        console.log('getAppSettingsList='+2);
        var setting = SettingsVal.getAppSettingsList()[index];
        return new SettingsVal(setting.date, setting.signalType, setting.cycles, setting.audio);
    }
};
SettingsVal.setVal = function(val){
    var h, m;
    var number = document.getElementById('number2');
    var hours = document.getElementById('hours2');
    var minutes = document.getElementById('minutes2');
    number.value = val;
    m = val % 3;
    h = (val - m) / 3;
    hours.value = h;
    if(m == 0){
        minutes.value = "00";
    }else {
        minutes.value = m * 20;
    }
};
function OptionList(){}
OptionList.close = function(){
    if(CountDown.isActive === false) {
        Player.stop();
        Player.loop = false;
    }
    $(".class_Settings_Data-popup").popup("close");
};
OptionList.CreateOptionList = function(idElement, files){
    $('#'+ idElement).empty();
    if (files == null || files.length == 0){
        $('.statusButton').hide();
        $('#'+ idElement).append('<p align="center">No option to show.</p>');
        return;
    }
    $('.statusButton').show();
    function getOptionHandler(file){
        return function optionHandler(){
            $(".class_Settings_Data-popup").popup("open");
            if(file.signalType == 4){
                $("#settingsAudio").show();
            }else{
                $("#settingsAudio").hide();
            }
            $('#settingsSignal').text(document.getElementById('signalType').options[file.signalType - 1].text);
            SettingsVal.setVal(file.cycles);
            if(file.audio !== "none") {
                var audio = JSON.parse(file.audio);
                $('#settingsAudio').text(audio.audioName);
                jQuery('#settingsAudio').unbind('click');
                jQuery('#settingsAudio').click(function(){
                    if (CountDown.isActive === false) {
                        if (Application.timer !== null) {
                            clearTimeout(Application.timer);
                        }
                        Player.stop();
                        Player.loop = false;
                        setTimeout(function () {
                            if (SaveData.returnDevice() === 'iOS') {
                                var path = Application.directoryPath + audio.audioPath;
                                path = path.replace("file://localhost/", "file:///");
                                path = decodeURI(path);
                                Player.playPause(path);
                            }else {
                                Player.playPause(audio.audioPath);
                            }
                            Player.loop = true;
                            console.log('Play');
                            Application.timer = setTimeout(function () {
                                Player.stop();
                                Player.loop = false;
                                console.log('Stop');
                            }, 10000);
                        }, 150);
                    } else {
                        Application.alert('Warning!!!','App in Active mode!!! Please wait while the active mode will end.');
                    }
                });
            }
            jQuery('#submitOption').unbind('click');
            jQuery('#submitOption').click(function(){
                if (CountDown.isActive === false) {
                    if (SaveData.returnSavedCycles() != file.cycles) {
                        if(CountDown.status == 'Sleep'){
                            Application.alert('Aborted','You can\'t set this option, cycle value not equal running option!!!');
                        }else {
                            if (SaveData.returnCountdownCycles() == SaveData.returnSavedCycles()) {
                                SaveData._db.setItem("signalType", file.signalType);
                                if (file.audio !== "none") {
                                    SaveData._db.setItem("audioForPlay", file.audio);
                                }
                                SaveData.setData();
                                OptionList.close();
                                $("#settingsListButton").collapsible("collapse");
                            } else {
                                $('<div>').simpledialog2({
                                    mode: 'button',
                                    themeDialog: SaveData.returnThemeID(),
                                    themeHeader: SaveData.returnThemeID(),
                                    headerText: 'Notification!',
                                    buttonPrompt: '<p>Different number of cycles, countdown will be started from the beginning. Are you sure?</p>',
                                    buttons: {
                                        'Yes': {
                                            click: function () {
                                                SaveData._db.setItem("cyclesCount", file.cycles);
                                                SaveData._db.setItem("countdownCycles", file.cycles);
                                                SaveData._db.setItem("signalType", file.signalType);
                                                if (file.audio !== "none") {
                                                    SaveData._db.setItem("audioForPlay", file.audio);
                                                }
                                                SaveData.setData();
                                                OptionList.close();
                                                $("#settingsListButton").collapsible("collapse");
                                            }
                                        },
                                        'No': {
                                            click: function () {
                                            },
                                            icon: "delete"
                                        }
                                    }
                                });
                            }
                        }
                    } else {
                        SaveData._db.setItem("signalType", file.signalType);
                        if (file.audio !== "none") {
                            SaveData._db.setItem("audioForPlay", file.audio);
                        }
                        SaveData.setData();
                        OptionList.close();
                        $("#settingsListButton").collapsible("collapse");
                    }
                }else{
                    Application.alert('Aborted!!!','You can\'t change settings when application in Active mode!!!');
                }
            });
        };
    }
    // Сохранение выбраного трека в локальном хранилище
    function getDeleteHandler(file){
        return function deleteHandler(){
            var oldLenght = SettingsVal.getAppSettingsList().length;
            var $parentUl = $(this).closest('ul');
            file = new SettingsVal(file.date, file.signalType, file.cycles, file.audio);
            var audio;
            if(file.signalType == '4'){
                audio = SaveData.returnAudioForPlay();
                audio = JSON.stringify(audio);
            }else{
                audio = "none";
            }
             if((SaveData.returnSignalType() === file.signalType)&&
                 (SaveData.returnSavedCycles() === file.cycles)&&
                 (audio.toUpperCase() === file.audio.toUpperCase() )){
                 if(CountDown.status == 'Awake') {
                     $('<div>').simpledialog2({
                         mode: 'button',
                         themeDialog: SaveData.returnThemeID(),
                         themeHeader: SaveData.returnThemeID(),
                         headerText: 'Delete option!',
                         buttonPrompt: '<p>This option now in use!!! Are you sure?</p>',
                         buttons: {
                             'Yes': {
                                 click: function () {
                                     file.deleteSetting();
                                     if (oldLenght === SettingsVal.getAppSettingsList().length + 1) {
                                         $(this).closest('li').remove();
                                         if (oldLenght == 1) {
                                             OptionList.CreateOptionList('settings_list', SettingsVal.getAppSettingsList());
                                         }
                                         $parentUl.listview('refresh');
                                     } else {
                                         console.log('Media not deleted. Something done wrong.');
                                         Application.alert('Error', 'Data not deleted. Something done wrong so please try again.');
                                     }
                                     var prom = SettingsVal.getAppSettingsList();
                                     OptionList.displayOptionList();
                                     if (prom.length >= 1) {
                                         var lastSetting = prom[prom.length - 1];
                                         SaveData._db.removeItem("cyclesCount");
                                         SaveData._db.removeItem("countdownCycles");
                                         SaveData._db.removeItem("signalType");
                                         SaveData._db.removeItem("audioForPlay");
                                         SaveData._db.removeItem("selectedAudio");
                                         SaveData._db.setItem("cyclesCount", lastSetting.cycles);
                                         SaveData._db.setItem("countdownCycles", lastSetting.cycles);
                                         SaveData._db.setItem("signalType", lastSetting.signalType);
                                         if (lastSetting.audio !== "none") {
                                             SaveData._db.setItem("audioForPlay", lastSetting.audio);
                                         }
                                         SaveData.setData();
                                     } else {
                                         SaveData._db.removeItem("cyclesCount");
                                         SaveData._db.removeItem("countdownCycles");
                                         SaveData._db.removeItem("signalType");
                                         SaveData._db.removeItem("audioForPlay");
                                         SaveData._db.removeItem("selectedAudio");
                                         SaveData.setData();
                                     }
                                 }
                             },
                             'No': {
                                 click: function () {
                                 },
                                 icon: "delete"
                             }
                         }
                     });
                 }else{
                     $('<div>').simpledialog2({
                         mode: 'button',
                         themeDialog: SaveData.returnThemeID(),
                         themeHeader: SaveData.returnThemeID(),
                         headerText: 'Delete option!',
                         buttonPrompt: '<p>You can not delete a running option. To remove it, select another option or go to the sleep mode!</p>',
                         buttons: {
                             'Ok': {
                                 click: function () {
                                 }
                             }
                         }
                     });
                 }
             }else{
                 file.deleteSetting();
                 if (oldLenght === SettingsVal.getAppSettingsList().length + 1){
                     $(this).closest('li').remove();
                     if (oldLenght == 1){
                         OptionList.CreateOptionList('settings_list', SettingsVal.getAppSettingsList());
                     }
                     $parentUl.listview('refresh');
                 } else{
                     console.log('Media not deleted. Something done wrong.');
                     Application.alert('Error','Data not deleted. Something done wrong so please try again.');
                 }
             }
        };
    }
    var $listElement, $linkElement;
    for(var i = 0; i < files.length; i++){
        $listElement = $('<li>');
        $linkElement = $('<a>');
        $linkElement
            .attr('href', '#')
            .text(files[i].date)
            .click(getOptionHandler(files[i]));

        $listElement.append($linkElement);

        $linkElement = $('<a>');
        $linkElement
            .attr('href', '#')
            .attr('data-icon', 'delete')
            .text('Delete')
            .click(getDeleteHandler(files[i]));

        $listElement.append($linkElement);

        $('#' + idElement).append($listElement);
    }
    $('#' + idElement).listview('refresh');
};
OptionList.displayOptionList = function(){
    OptionList.CreateOptionList('settings_list', SettingsVal.getAppSettingsList());
};
