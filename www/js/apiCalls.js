window.addEventListener("orientationchange", orientationChange, true);
function orientationChange(e) {
    var orientation="portrait";
    switch(window.orientation)
    {
        case -90:
        case 90:
            orientation = "landscape";
            document.getElementById('sleepFace').style.width = "40%";
            break;
        default:
            orientation = "portrait";
            document.getElementById('sleepFace').style.width = "80%";
            break;
    }
}
function isNumberKey(evt){
    var charCode = (evt.which) ? evt.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
    return true;
}
document.addEventListener("deviceready", onDeviceReady, false);
function onDeviceReady(){
    if(device.platform === 'iOS'){
        SaveData._db.setItem("device", device.platform);
        $('.exitBtn').hide();
    }else{
        $("#addFiles").hide();
    }
    var optionList = SettingsVal.getAppSettingsList();
    if (optionList == null || optionList.length == 0){
        $('.statusButton').hide();
    }else{
        $('.statusButton').show();
    }
    Application.initFilesListPage();
    var cycles = SaveData.returnSavedCycles();
    if ((cycles == '')||(cycles === '')||(cycles == null)||(cycles === null)) {
        console.log('Cycles value was not found!!!');
    }else{
        if((SaveData.returnCountdownCycles() > 0)&&(SaveData.returnCountdownCycles() !== null)){

        }else {
            _db.setItem("countdownCycles", SaveData.returnSavedCycles());
        }
    }
    var themeClass;
    if(SaveData.returnThemeID() === null){
        themeClass = 'b';
        SaveData._db.setItem('themeID', themeClass);
    }
    else{
        themeClass = SaveData.returnThemeID();
    }
    $("#"+themeClass).attr("checked",true).checkboxradio("refresh");
    var release = SaveData._db.getItem("selectedRelease");
    if((release === 'basic')||((release == '')||(release === '')||(release == null)||(release === null))){
        if((release == '')||(release === '')||(release == null)||(release === null)){
            release = 'basic';
            SaveData._db.setItem('selectedRelease', release);
        }
        $("#"+release).attr("checked",true).checkboxradio("refresh");
        $("#notification").slideDown();
    }else{
        $("#"+release).attr("checked",true).checkboxradio("refresh");
        $("#notification").slideUp();
    }
    $( ".page" ).page({
        contentTheme: themeClass
    });
    $( '.page' ).removeClass( "ui-overlay-a ui-overlay-b ui-overlay-c ui-overlay-d ui-overlay-e ui-overlay-f" ).addClass( "ui-overlay-" + themeClass );
    $( '.page' ).removeClass( "ui-page-theme-a ui-page-theme-b ui-page-theme-c ui-page-theme-d ui-page-theme-e ui-page-theme-f" ).addClass( "ui-page-theme-" + themeClass );
    $( "#container" ).removeClass( "ui-overlay-a ui-overlay-b ui-overlay-c ui-overlay-d ui-overlay-e ui-overlay-f" ).addClass( "ui-overlay-" + themeClass );
    $( ".ui-collapsible-content" ).removeClass( "ui-body-a ui-body-b ui-body-c ui-body-d ui-body-e ui-body-f" ).addClass( "ui-body-" + themeClass );
    $( ".theme" ).text( themeClass );
    $.mobile.changeGlobalTheme(themeClass);
    // Android customization
    cordova.plugins.backgroundMode.setDefaults({ silent: true});
    // Enable background mode
    cordova.plugins.backgroundMode.enable();
    // Called when background mode has been activated
    cordova.plugins.backgroundMode.onactivate = function () {
        console.log("backgroundMode.onactivate");
        setTimeout(function () {
            // Modify the currently displayed notification
            cordova.plugins.backgroundMode.configure({
                silent: true
            });
        }, 5000);
    };
}
$(document).one('mobileinit', function () {
    $.mobile.pageContainer = $('#container');
    $.mobile.defaultPageTransition = 'slide';
    $.mobile.autoInitializePage = false;
});
$(document).on('pagebeforeshow', function(){
    var themeClass2;
    if(SaveData.returnThemeID() === null){
        themeClass2 = 'b';
        SaveData._db.setItem('themeID', themeClass2);
    }
    else{
        themeClass2 = SaveData.returnThemeID();
    }
    $( ".page" ).page({
        contentTheme: themeClass2
    });
    $( "#container" ).removeClass( "ui-overlay-a ui-overlay-b ui-overlay-c ui-overlay-d ui-overlay-e ui-overlay-f" ).addClass( "ui-overlay-" + themeClass2 );
    $( '.page' ).removeClass( "ui-overlay-a ui-overlay-b ui-overlay-c ui-overlay-d ui-overlay-e ui-overlay-f" ).addClass( "ui-overlay-" + themeClass2 );
    $( '.page' ).removeClass( "ui-page-theme-a ui-page-theme-b ui-page-theme-c ui-page-theme-d ui-page-theme-e ui-page-theme-f" ).addClass( "ui-page-theme-" + themeClass2 );
});
$(document).on('pagebeforeshow', '#settings', function(){
    SaveData.setData();
});
$(document).ready(function() {
    window.location.hash = 'settings';
    $.mobile.initializePage();
});
jQuery.extend(jQuery.mobile,{
        ajaxEnabled: false
    });
$(document).on('change','#slider', function(){
    submitSlide();
    SaveData.setVal(this);
});
$(document).on('change','#number', function(){
    submitSlide();
    SaveData.setVal(this);
});
$(document).on('change','#hours', function(){
    submitSlide();
    SaveData.setVal(this);
});
$(document).on('change','#minutes', function(){
    submitSlide();
    SaveData.setVal(this);
});
function submitSlide(){
    if ( $( "#submitField" ).is( ":hidden" ) ) {
        $("#submitField").slideDown();
        $( "#startButton").slideUp();
    }
}
$(document).on('change','#signalType', function(){
    var value = this.value;
    submitSlide();
    if((value =="1")||(value =="4")||(value =="5")){
        Application.alert('Notification!!!','Sound doesn\'t play when phone in silent mode!');
    }
    switch (value){
        case "4":
            $("#musicListButton").slideDown();
            if(device.platform === 'iOS') {
                $("#addFiles").slideDown();
            }
            Application.displayAudioList();
            break;
        case "3":
            $( "#musicListButton" ).slideUp();
            $("#addFiles").slideUp();
            window.plugins.flashlight.available(function(isAvailable) {
                console.log(isAvailable);
                if (!isAvailable){
                    console.log('Flashlight not available on this device');
                    Application.alert('Flashlight!!!','This option not available on this device!!! Please select another option.');
                }else{
                    if(device.platform === 'iOS') {
                        Application.alert('Flashlight!!!', 'Flashlight doesn\'t work when Application in background!');
                    }
                }
            });
            break;
        case "1":
        case "2":
        case "5":
        default :
            $( "#musicListButton" ).slideUp();
            $("#addFiles").slideUp();
    }
});
$(document).on('change','#flip_1', function(){
    window.localStorage.setItem("noticeActive", this.value);
});
$( document ).on( "pagecreate", function() {
    $( "#theme-selector input" ).on( "change", function( event ) {
        var themeClass = $( "#theme-selector input:checked" ).attr( "id" );
        $( ".page" ).page({
            contentTheme: themeClass
        });
        $( '.page' ).removeClass( "ui-overlay-a ui-overlay-b ui-overlay-c ui-overlay-d ui-overlay-e ui-overlay-f" ).addClass( "ui-overlay-" + themeClass );
        $( '.page' ).removeClass( "ui-page-theme-a ui-page-theme-b ui-page-theme-c ui-page-theme-d ui-page-theme-e ui-page-theme-f" ).addClass( "ui-page-theme-" + themeClass );
        $( "#container" ).removeClass( "ui-overlay-a ui-overlay-b ui-overlay-c ui-overlay-d ui-overlay-e ui-overlay-f" ).addClass( "ui-overlay-" + themeClass );
        $( ".ui-collapsible-content" ).removeClass( "ui-body-a ui-body-b ui-body-c ui-body-d ui-body-e ui-body-f" ).addClass( "ui-body-" + themeClass );
        $( ".theme" ).text( themeClass );
        $.mobile.changeGlobalTheme(themeClass);
        Application.theme = themeClass;
        SaveData._db.setItem('themeID', themeClass);
    });
    $( "#releaseSelector input" ).on( "change", function( event ) {
        var release = $( "#releaseSelector input:checked" ).attr( "id" );
        if (release === 'basic'){
            $("#notification").slideDown();
        }else if(release === 'advanced'){
            $("#notification").slideUp();
        }
        SaveData._db.setItem('selectedRelease', release);
    });
});

$.mobile.changeGlobalTheme = function(theme){
    var themes = " a b c d e f";
    function setTheme(cssSelector, themeClass, theme)
    {
        $(cssSelector)
            .removeClass(themes.split(" ").join(" " + themeClass + "-"))
            .addClass(themeClass + "-" + theme)
            .attr("data-theme", theme);
    }
    setTheme(".ui-mobile-viewport", "ui-overlay", theme);
    setTheme("[data-role='page']", "ui-body", theme);
    setTheme("[data-role='header']", "ui-bar", theme);
    setTheme("[data-role='listview'] > li", "ui-bar", theme);
    setTheme(".ui-btn", "ui-btn-up", theme);
    setTheme(".ui-btn", "ui-btn-hover", theme);
};
function plsuccess(data){
    Application.src = data[0].exportedurl;
    Application.src = Application.src.replace("file://localhost/","/");
    Application.src = decodeURI(Application.src);
    Application.initFilesListPage();
    var appFile;
    var slen = data.length;
    for (var i = 0; i < slen; i++){
        var test = data[i].exportedurl;
        test = test.replace(Application.directoryPath, '');
        appFile = new AppFile(data[i].filename, test);
        appFile.addFile();
    }
    SaveData._db.removeItem("selectedAudio");
    $('#musicListButton .ui-collapsible-heading-toggle').text("Choose a music composition...");
    var signalType = document.getElementById('signalType');
    signalType.value = '4';
    $('#signalType').selectmenu('refresh');
    $("#musicListButton").slideDown();
    if(device.platform === 'iOS'){
        $("#addFiles").slideDown();
    }
    Application.displayAudioList();
    if(slen > 1) {
        Application.alert('Notification message!', slen + ' files was added to User music list.');
    }else{
        Application.alert('Notification message!', 'File was added to User music list.');
    }
    $( "#submitField" ).slideDown();
}
function delError(e){
    Application.alert('Adding File Error!!!',JSON.stringify(e));
}
function MediaColect(){
    window.plugins.iOSAudioPicker.getAudio(plsuccess,delError,'false','false');
}
function MediaColect2(){
    window.plugins.iOSAudioPicker.getAudio(plsuccess,delError,'true','false');
}
function PlayerStop(){
    if(CountDown.isActive === false) {
        clearTimeout(Application.timer);
        Player.stop();
        Player.loop = false;
    }
}
function addingAlert(){
    Application.alert('Notification','After files be added you see Notification message!');
}
$(document).on('click','#cancelButton', function(){
    var optionList = SettingsVal.getAppSettingsList().length + 1;
    if (optionList == 1){
        Application.alert('Alert!!!', 'You not submit Options!!!');
    }else {
        SaveData.setData();
        PlayerStop();
    }
});
$(document).on('click','#sleepFace', function(){
    var optionList = SettingsVal.getAppSettingsList().length + 1;
    if (optionList == 1){
        Application.alert('Alert!!!', 'You not submit Options!!!');
    }else {
        CountDown.statusEvent();
    }
});
$(document).on('click','#startButtonText', function(){
    var startButton = document.getElementById("startButtonText");
    if (CountDown.status == 'Awake'){
        CountDown.stop();
        CountDown.start();
        startButton.textContent = 'Stop';
        location.replace("#mode");
    }else{
        CountDown.stop();
        startButton.textContent = 'Start';
    }
});