/**
 * Created by Dell-STO on 25.02.2015.
 */
var CountDown={
    _db: window.localStorage,
    timer: null,
    blinking: null,
    interval: null,
    status: 'Awake',
    isActive: false,
    type: null,
    watchID: null,
    Run: function(second, typeCountDown){
        CountDown.type = typeCountDown;
        var now = new Date();
        var endMinute=0,endHour=0,endDay=0,endMonth=0,endYear=0;
        second = second || now.getSeconds();
        second = second + now.getSeconds();
        endYear =  endYear || now.getFullYear();
        endMonth = endMonth ? endMonth - 1 : now.getMonth();    //номер месяца начинается с 0
        endDay = endDay || now.getDate();
        endHour = endHour || now.getHours() ;
        endMinute = endMinute || now.getMinutes();
        //добавляем секунду к конечной дате (таймер показывает время уже спустя 1с.)
        var endDate = new Date(endYear,endMonth,endDay,endHour,endMinute,second+1);
        CountDown.interval = setInterval(function() {
        //запускаем таймер с интервалом 1 секунду

            var time = endDate.getTime() - now.getTime();
            if (time < 0) {                      //если конечная дата меньше текущей
                clearInterval(CountDown.interval);
                Application.alert('Timer Error!!!',"Incorect date!");
            } else {
                var days = Math.floor(time / 864e5);
                var hours = Math.floor(time / 36e5) % 24;
                var minutes = Math.floor(time / 6e4) % 60;
                var seconds = Math.floor(time / 1e3) % 60;
                if(+seconds < 10){
                    seconds = '0' + seconds;
                }
                //
                //Формат отображение часов
                //шрифт и размер текста
                var digit='<div class="time-item">'+'<div class="time-inner">';
                var text='</div><div>';
                var headerText='';
                if(typeCountDown == 20){
                    var number = SaveData.returnSavedCycles() - SaveData.returnCountdownCycles() +1;
                    headerText = 'Time to the '+number+' of '+SaveData.returnSavedCycles()+' cycle remains:';
                }else{
                    headerText = 'Active mode time left:';
                }
                // значение и текст часов
                var end = '</div></div><span class="time-delimiter">:</span>';
                document.getElementById('mytimer2').innerHTML = '<div class="abs"><div class="table"><div class="cell"><div class="time-holder"><h3>'+ headerText +'</h3>' +
                digit + minutes + text + '' + end + digit + seconds + text + '</div></div></div></div>';
                //цыкличный вызов братного отсчета
                seconds = +seconds;
                if (!seconds && !minutes && !days && !hours) {
                    clearInterval(CountDown.interval);
                    if(typeCountDown == 20) {
                        var release = SaveData._db.getItem("selectedRelease");
                        if (release == 'basic'){
                            CountDown.accelerometerStop();
                            CountDown.startActive();
                        }
                        else{
                            CountDown.accelerometerStop();
                            CountDown.accelerometerStart();
                        }
                    }
                    else if(typeCountDown == 2){
                        CountDown.startSedentary();
                    }
                }
            }
            now.setSeconds(now.getSeconds() + 1); //увеличиваем текущее время на 1 секунду
        }, 1000);
    },
    startActive: function(){
        $( "#mode" ).removeClass( "ui-overlay-a ui-overlay-b ui-overlay-c ui-overlay-d ui-overlay-e ui-overlay-f sedentaryBlink " ).addClass( "ui-overlay-" + SaveData.returnThemeID() );
        var release = SaveData._db.getItem("selectedRelease");
        if((document.getElementById('flip_1').value == "on")&&(release === 'basic')){
            CountDown.interval = setTimeout(function(){
                var vibrateArray = [0, 100, 100, 200, 100, 400, 100, 800];
                var i = 0;
                CountDown.interval =setTimeout(function run(){
                    if(i < vibrateArray.length){
                        navigator.vibrate(vibrateArray[i]);
                        i = i+1;
                        CountDown.interval = setTimeout(run, 500);
                    }else{
                        i = 0;
                        CountDown.interval = setTimeout(run, 60000);
                    }
                },500);
                $('<div>').simpledialog2({
                    mode: 'button',
                    themeDialog: SaveData.returnThemeID(),
                    themeHeader: SaveData.returnThemeID(),
                    headerText: 'Notice',
                    buttonPrompt:'<p>Do you want enable active mode?</p>',
                    buttons : {
                        'OK': {
                            click: function () {
                                console.log('Ok');
                                $('#mytimer').hide();
                                $('#mytimer2').show();
                                clearInterval(CountDown.interval);
                                CountDown.Run(120,2);
                                CountDown.generateSignal();
                                location.replace("#mode");
                            }
                        },
                        'Cancel': {
                            click: function () {
                                console.log('Cancel');
                                clearInterval(CountDown.interval);
                                //CountDown.Run(60, 20);
                                CountDown.Run(1200,20);
                            },
                            icon: "delete"
                        }
                    }
                });
                }, 150);
        }else{
            clearInterval(CountDown.interval);
            CountDown.Run(120,2);
            $('#mytimer2').show();
            CountDown.generateSignal();
            location.replace("#mode");
        }
    },
    accelerometerStop: function(){
        clearInterval(CountDown.blinking);
        navigator.accelerometer.clearWatch(CountDown.watchID);
        CountDown.watchID = null;
        $( "#mode" ).removeClass( "ui-overlay-a ui-overlay-b ui-overlay-c ui-overlay-d ui-overlay-e ui-overlay-f sedentaryBlink" ).addClass( "ui-overlay-" + SaveData.returnThemeID() );
    },
    accelerometerStart: function(){
        var macive = [];
        var start = [0, 0 ,0];
        var prom = [0, 0 ,0];
        var timerBlinck =0;
        var time = 0;
        var isBlik = false;
        var deltaTime = 0;
        var tDeltaTime = 0;
        function onAccelSuccess(acceleration) {
            clearInterval(CountDown.blinking);
            var testVal = 15 - timerBlinck;
            if (testVal != 0){

            }else{

            }
            if(macive.length >= 1){
                prom[0] = macive[macive.length - 1][0] - acceleration.x;
                prom[1] = macive[macive.length - 1][1] - acceleration.y;
                prom[2] = macive[macive.length - 1][2] - acceleration.z;
                if((prom[0] > 0.4)||(prom[1] > 0.4)||(prom[2] > 0.4)){
                    start[0] = acceleration.x;
                    start[1] = acceleration.y;
                    start[2] = acceleration.z;
                    macive.push(start);
                    timerBlinck = timerBlinck+1;

                }else{
                    start[0] = acceleration.x;
                    start[1] = acceleration.y;
                    start[2] = acceleration.z;
                    macive.push(start);
                    if(timerBlinck > 0){
                        timerBlinck = timerBlinck -1;
                    }
                }
            }
            else{
                $("#mode").toggleClass("sedentaryBlink");
                start[0] = acceleration.x;
                start[1] = acceleration.y;
                start[2] = acceleration.z;
                macive.push(start);
                timerBlinck =0;
            }
            var digit='<div class="time-item">'+
                '<div class="time-inner">';

            var text='</div><div>';
            var end = '</div></div><span class="time-delimiter">:</span>';
            document.getElementById('mytimer2').innerHTML = '<div class="abs"><div class="table"><div class="cell"><div class="time-holder">' +
            digit + timerBlinck + text + '</div></div></div></div>';

            CountDown.blinking = setInterval(function(){

                if(time >= timerBlinck){
                    time = timerBlinck
                }else {
                    time = time + 0.1;
                }
                deltaTime = (14-time)/14;
                tDeltaTime += 0.1;
                if(tDeltaTime>=deltaTime){
                    tDeltaTime = 0;
                    isBlik = true;
                    $("#mode").toggleClass("sedentaryBlink");
                }
                if (isBlik&&tDeltaTime>=0.2) {
                    $("#mode").toggleClass("sedentaryBlink");
                    isBlik = false;
                }
                if(time >= 14){
                    clearInterval(CountDown.blinking);
                    CountDown.startActive();
                }
            }, 100);

            if(timerBlinck >= 15){
                stopWatch();
            }
        }

        function onError() {
            Application.alert('Accelerometer' ,'Error!');
        }

        var options = { frequency: 1000 };

        CountDown.watchID = navigator.accelerometer.watchAcceleration(onAccelSuccess, onError, options);

        function stopWatch() {
            navigator.accelerometer.clearWatch(CountDown.watchID);
            CountDown.watchID = null;
            $( "#mode" ).removeClass( "ui-overlay-a ui-overlay-b ui-overlay-c ui-overlay-d ui-overlay-e ui-overlay-f sedentaryBlink" ).addClass( "ui-overlay-" + SaveData.returnThemeID() );
        }
    },
    startSedentary: function(){
        var modeHeader = document.getElementById("modeHeader");
        modeHeader.textContent = 'Sedentary';
        var modeName;
        var modeText;
        if((SaveData.returnCountdownCycles() !== null)&&(SaveData.returnCountdownCycles() > 0)) {
            CountDown._db.setItem("countdownCycles", (SaveData.returnCountdownCycles() - 1));
            console.log(SaveData.returnCountdownCycles() == 0);
            if (SaveData.returnCountdownCycles() == 0){
                CountDown.stopSignal();
                CountDown.interval = setTimeout(function(){
                    var vibrateArray = [0, 1500, 500,500, 500, 500, 500, 1500];
                    var i = 0;
                    CountDown.interval =setTimeout(function run(){
                        if(i < vibrateArray.length){
                            navigator.vibrate(vibrateArray[i]);
                            i = i+1;
                            CountDown.interval = setTimeout(run, 500);
                        }else{
                            i = 0;
                            CountDown.interval = setTimeout(run, 60000);
                        }

                    },500);
                    var release = SaveData._db.getItem("selectedRelease");
                    SaveData._db.setItem("countdownCycles", SaveData.returnSavedCycles());
                    var startButton = document.getElementById("startButtonText");
                    startButton.textContent = 'Start';
                    if (release === 'basic'){
                        basicCongratulation();
                    }
                    else{
                        advancedCongratulation();
                    }
                }, 150);
            }else{
                CountDown.stopSignal();
                CountDown.Run(1200, 20);
                //CountDown.Run(60, 20);
                $('#mytimer2').show();
                console.log(CountDown._db.getItem("countdownCycles"));
                location.replace("#mode");
                modeName = document.getElementsByClassName("mode2");
                modeText = 'Sedentary';
                for (var i = 0; i < modeName.length; i++){
                    modeName[i].textContent = modeText;
                }
                $(".mode2").buttonMarkup({ icon: "minus" });
            }
        }
        else{
            var slider = document.getElementById("slider").value;
            CountDown._db.setItem("countdownCycles", (slider - 1));
            CountDown.Run(1200, 20);
            //CountDown.Run(60, 20);
            console.log(CountDown._db.getItem("countdownCycles"));
            CountDown.stopSignal();
            location.replace("#mode");
            modeName = document.getElementsByClassName("mode2");
            modeText = 'Sedentary';
            for (var i = 0; i < modeName.length; i++){
                modeName[i].textContent = modeText;
            }
            $(".mode2").buttonMarkup({ icon: "minus" });
        }
    },
    generateSignal: function(){
        var modeHeader = document.getElementById("modeHeader");
        modeHeader.textContent = 'Active';
        $(".mode2").buttonMarkup({ icon: "plus" });
        var modeName = document.getElementsByClassName("mode2");
        var modeText = 'Active';
        for (var i = 0; i < modeName.length; i++){
            modeName[i].textContent = modeText;
        }
        CountDown.isActive = !CountDown.isActive;
        clearTimeout(Application.timer);
        Player.loop = false;
        Player.stop();
        CountDown.blinking=setInterval(function(){
            $("#mode").toggleClass("activeBlink");
        },350);
        var path = null;
        var signalValue = CountDown._db.getItem("signalType");
        switch (signalValue) {
            case '2':
                console.log('Start Vibrate');
                CountDown.timer = setTimeout(function run() {
                    navigator.vibrate(500);
                    CountDown.timer = setTimeout(run, 1000);
                }, 100);
                break;
            case '3':
                console.log('Start Flash');
                window.plugins.flashlight.available(function (isAvailable) {
                    console.log(isAvailable);
                    if (isAvailable) {
                        CountDown.timer = setTimeout(function run() {
                            window.plugins.flashlight.switchOn();
                            setTimeout(window.plugins.flashlight.switchOff(), 150);
                            CountDown.timer = setTimeout(run, 300);
                        }, 500);

                    } else {
                        console.log('Flashlight not available on this device');
                        Application.alert('Flashlight!!!','This option not available on this device');
                    }
                });

                break;
            case '4':
                console.log('Start Music');
                var file = JSON.parse(CountDown._db.getItem("audioForPlay"));
                Player.loop = true;
                if (device.platform === 'iOS') {
                    path = Application.directoryPath + file.audioPath;
                    path = path.replace("file://localhost/", "file:///");
                    path = decodeURI(path);
                    Player.playPause(path);
                }else {
                    Player.playPause(file.audioPath);
                }
                break;
            case '5':
                console.log('Start Sound');
                path = location.origin + location.pathname;
                path = path.replace('index.html', 'sounds/I like to move it.mp3');
                Player.loop = true;
                Player.playPause(path);
                break;
                break;
            case '1':
            default :
                console.log('Start Beep');
                path = location.origin + location.pathname;
                path = path.replace('index.html', 'beep.wav');
                CountDown.timer = setTimeout(function run(){
                    Player.loop = false;
                    Player.playPause(path);
                    CountDown.timer = setTimeout(run, 1500);
                }, 1500);
        }
    },
    stopSignal: function(){
        CountDown.isActive = false;
        clearInterval(CountDown.blinking);
        $(".mode2").buttonMarkup({ icon: "minus" });
        $( "#mode" ).removeClass( "ui-overlay-a ui-overlay-b ui-overlay-c ui-overlay-d ui-overlay-e ui-overlay-f activeBlink sedentaryBlink" ).addClass( "ui-overlay-" + SaveData.returnThemeID() );
        Player.loop = false;
        Player.stop();
        clearTimeout(CountDown.timer);
    },
    start: function(){
        CountDown.stop();
        if(SaveData.returnSavedCycles() === null){
            SaveData.saveData();
        }
        var statusButton = document.getElementsByClassName("statusButton");
        CountDown.status = 'Sleep';
        $("#sleepFace").hide();
        $( "#mode" ).removeClass( "modeContent" );
        for (var i = 0; i < statusButton.length; i++){
            statusButton[i].textContent = CountDown.status;
        }
        CountDown.Run(1200, 20);
        //CountDown.Run(60, 20);
        $('#mytimer2').show();
        var modeHeader = document.getElementById("modeHeader");
        modeHeader.textContent = 'Sedentary';
        $(".mode2").buttonMarkup({ icon: "minus" });
        var modeName = document.getElementsByClassName("mode2");
        var modeText = 'Sedentary';
        for (var i = 0; i < modeName.length; i++){
            modeName[i].textContent = modeText;
        }
        location.replace("#mode");
    },
    stop: function(){
        $('#mytimer2').hide();
        var statusButton = document.getElementsByClassName("statusButton");
        CountDown.status = 'Awake';
        for (var i = 0; i < statusButton.length; i++){
            statusButton[i].textContent = CountDown.status;
        }
        $("#sleepFace").show();
        $("#mode").toggleClass("modeContent");
        var modeHeader = document.getElementById("modeHeader");
        modeHeader.textContent = 'Sleep';
        CountDown.accelerometerStop();
        CountDown.stopSignal();
        var modeName = document.getElementsByClassName("mode2");
        var modeText = 'Sleep';
        for (var i = 0; i < modeName.length; i++){
            modeName[i].textContent = modeText;
        }
        $(".mode2").buttonMarkup({ icon: "recycle" });
        clearInterval(CountDown.interval);
    },
    statusEvent: function(){
        console.log(CountDown.status);
        var startButton = document.getElementById("startButtonText");
        if (CountDown.status == 'Awake'){
            CountDown.stop();
            CountDown.start();
            startButton.textContent = 'Stop';
        }
        else{
            CountDown.stop();
            startButton.textContent = 'Start';
        }
    },
    share: function(){
        var share_Via = document.getElementById('share_Via').value;
        var textarea_1 = document.getElementById('textarea_1').value;
        switch (share_Via){
            case '1':
                window.plugins.socialsharing.shareViaFacebookWithPasteMessageHint(textarea_1, null /* img */, null /* url */, 'Paste it dude!', function() {console.log('share ok')}, function(errormsg){console.log(errormsg)});
                break;
            case '2':
                window.plugins.socialsharing.shareViaTwitter(textarea_1);
                break;
            case '3':
                window.plugins.socialsharing.shareViaFacebookWithPasteMessageHint(textarea_1, null /* img */, null /* url */, 'Paste it dude!', function() {console.log('share ok')}, function(errormsg){console.log(errormsg)});
                window.plugins.socialsharing.shareViaTwitter(textarea_1);
                break;
            default:
        }
        CountDown.stop();
        location.replace("#settings");
        var modeName = document.getElementsByClassName("mode2");
        var modeText = 'Sleep';
        for (var i = 0; i < modeName.length; i++){
            modeName[i].textContent = modeText;
        }
        $(".mode2").buttonMarkup({ icon: "recycle" });
    }
};
function advancedCongratulation(){
    $('<div>').simpledialog2({
        mode: 'button',
        themeDialog: SaveData.returnThemeID(),
        themeHeader: SaveData.returnThemeID(),
        headerText: 'Congratulation',
        dialogAllow: true,
        dialogForce: true,
        buttonPrompt:'<label for="textarea_1">Message:</label>'+
        '<textarea name="textarea_1" id="textarea_1">I completed my #20/2 Active goal today</textarea>'+
        ' <label for="share_Via">Share via:</label>'+
        '<select id="share_Via" class="share_Via" name="share_Via" data-role="select">'+
        '<option value="1">Facebook</option>'+
        '<option value="2">Twitter</option>'+
        '<option value="3">Facebook and Twitter</option>'+
        '</select>'+
        '<a href="#" class="ui-btn ui-shadow ui-corner-all" onclick="clearTimeout(CountDown.interval), CountDown.share()">Share</a>'                        ,
        buttons : {
            'Skip': {
                click: function () {
                    console.log('Skip');
                    clearTimeout(CountDown.interval);
                    CountDown.stop();
                    var modeName = document.getElementsByClassName("mode2");
                    var modeText = 'Sleep';
                    for (var i = 0; i < modeName.length; i++){
                        modeName[i].textContent = modeText;
                    }
                    $(".mode2").buttonMarkup({ icon: "recycle" });
                }
            }
        }
    });
}
function basicCongratulation(){
    $('<div>').simpledialog2({
        mode: 'button',
        themeDialog: SaveData.returnThemeID(),
        themeHeader: SaveData.returnThemeID(),
        headerText: 'Congratulation',
        dialogAllow: true,
        dialogForce: true,
        buttonPrompt:'<h2>You completed #20/2 Active goal today</h2>',
        buttons : {
            'Ok': {
                click: function () {
                    console.log('Ok');
                    CountDown.stop();
                }
            }
        }
    });
}
