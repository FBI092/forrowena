///**
// * Created by Dell-STO on 11.03.2015.
// */

    var files = [];
    var _tableName = 'files';
    var _db = window.localStorage;
function AppFile (name, fullPath){
    this.audioName = name;
    this.audioPath = fullPath;
    this.save = function(files){
        return _db.setItem(_tableName, JSON.stringify(files));
        };
    this.load =function(){
        return JSON.parse(_db.getItem(_tableName));
    };
}
AppFile.save =function(files){
    return _db.setItem(_tableName, JSON.stringify(files));
};
AppFile.load =function(){
    return JSON.parse(_db.getItem(_tableName));
};

AppFile.prototype.addFile = function(){
    var index = AppFile.getIndex(this.audioPath);
    files = AppFile.getAppFiles();
    if(index === false){
        files.push(this);
        //console.log(files.length);
        //for(var i in files){console.log(files[i].audioName+' '+files[i].audioPath);}
    }else{
        files.splice(index, 1);
        this.save(files);
    }
    return files;
};

AppFile.EXTENSIONS = ['.mp3', '.wav', '.m4a'];

AppFile.prototype.deleteFile =function(){
    var index = AppFile.getIndex(this.audioPath);
    files = AppFile.getAppFiles();
    if(index !== false){
        files.splice(index, 1);
        this.save(files);
    }
    return files;
};

AppFile.prototype.compareTo = function(other){
    return AppFile.compare(this, other);
};

AppFile.compare = function(appFile, other){
    if(other == null){
        return 1;
    }else if (appFile == null){
        return -1;
    }
    return appFile.audioName.localeCompare(other.audioName);
};

AppFile.prototype.compareToIgnoreCase = function(other){
    return AppFile.compareIgnoreCase(this, other);
};

AppFile.compareIgnoreCase = function(appFile, other){
    if (other == null){return 1;}
    else if (appFile == null){return -1;}
    return appFile.audioName.toUpperCase().localeCompare(other.audioName.toUpperCase());
};

AppFile.getAppFiles = function()
{
    files = AppFile.load();
    if((files == '')||(files === '')||(files == null)||(files === null)){
        console.log('getAppFiles return null');
        return [];
    }else{
        //console.log('getAppFiles return files');
        return files;
    }
    //return (files === null) ? [] : files;
};

AppFile.getAppFile = function(path){
    var index = AppFile.getIndex(path);
    console.log(index);
    if(index === false){
        console.log('getAppFile='+1);
        return null;
    }else{
        console.log('getAppFile='+2);
        var file = AppFile.getAppFiles()[index];
        return new AppFile(file.audioName, file.audioPath);
    }
};

AppFile.getIndex = function(path){
    files = AppFile.getAppFiles();
    for(var i = 0; i < files.length; i++){
        if(files[i].audioPath.toUpperCase() === path.toUpperCase()){
            return i;
        }
    }
    return false;
};
AppFile.deleteFiles = function () {
    this.save([]);
    console.log('Files was deleted');
};