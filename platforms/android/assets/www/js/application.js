/**
 * Created by Dell-STO on 11.03.2015.
 */
//document.addEventListener("deviceready", onDeviceReady, false);
//function onDeviceReady(){
//    console.log('onDeviceReady');
//    Application.initFilesListPage();
//}
var Application = {
    initApplication: function() {
        $(document).on(
            'pageinit',
            '#index',
            function(){
                //Переход на Media List
                console.log('go to Media List');
                Application.initFilesListPage();
            }
        );
        $(document).on(
            'pagechange',
            function(event, propertirs){
                if (propertirs.absUrl === $.mobile.path.makeUrlAbsolute('player.html')){
                    Application.initPlayerPage(
                        JSON.parse(propertirs.options.data.file)
                    );
                }
            }
        );
    },
    initFilesListPage: function(){
        console.log('Start search1');
        $(".class_Waiting-popup").popup("open");
        setTimeout(function(){
                            Application.updateMediaList();
                        }, 150);
        //$(document).onload("#settings", function(){
        //        console.log('Start search2');
        //        //$('#waiting-popup').popup('open');
        //        setTimeout(function(){
        //            Application.updateMediaList();
        //        }, 150);
        //    }
        //);
        $(document).on('endupdate', function(){
            console.log('Stop search');
            console.log(AppFile.getAppFiles());
            countDown(60, 20);
            setCyclesFormElement();
            $('.class_Waiting-popup').popup('close');
            Application.createFilesList('files-list',AppFile.getAppFiles());
        });
        Application.createFilesList('files-list',AppFile.getAppFiles());
    },
    initPlayerPage: function(file){
        Player.stop();
        $('#media-name').text(file.audioName);
        $('#media-path').text(file.audioPath);
        $('#player-play').click(function(){
            console.log('Test player');
            Player.playPause(file.audioPath);
        });
        $('#player-stop').click(function(){
            console.log('Test player stop');
            Player.stop();
        });
    },
    updateIcons: function(){
        if ($(window).width() > 480){
            $('a[data-icon], button[data-icon]').each(function(){
                $(this).removeAttr('data-iconpos');
            });
        }else{
            $('a[data-icon], button[data-icon]').each(function(){
                $(this).attr('data-iconpos', 'notext');
            });
        }
    },
    openLinksInApp: function(){
        $("a[target=\"_blank\"]").on('click', function(event){
            event.preventDefault();
            window.open($(this).attr('href'), '_target');
        });
    },

    updateMediaList: function(){
        window.requestFileSystem(
            LocalFileSystem.PERSISTENT,
            0,
            function(fileSystem){
                var root = fileSystem.root;
                //AppFile.deleteFiles();
                console.log(media.volume);
                Application.collectMedia(root, true);
            },
            function(error){
                alert('File System Error: ' + error.code);
            }
        );
    },
    collectMedia: function(path, recursive, level){
        if(level === undefined){
            level = 0;
        }
        var directoryEntry = new DirectoryEntry('', path.fullPath);
        if(!directoryEntry.isDirectory){
            alert('The provided path is not directory');
            return;
        }

        path.createReader().readEntries(function(entries){
                var appFile;
                var extension;
            for (var i = 0; i < entries.length; i++){
                if (entries[i].name === '.'){
                    continue;
                }
                extension = entries[i].name.substr(entries[i].name.lastIndexOf('.'));
                if (entries[i].isDirectory === true && recursive === true){
                    setTimeout(Application.collectMedia(entries[i], recursive, level +1),150);
                }else if (entries[i].isFile === true && $.inArray(extension, AppFile.EXTENSIONS) >= 0){
                    appFile = new AppFile(entries[i].name, entries[i].nativeURL);
                    var files=appFile.addFile();
                    //for (var J in files){console.log(J+' '+files[J].audioName+' '+files[J].audioPath);}
                    appFile.save(files);
                    //console.log('File saved: '+ entries[i].nativeURL);
                }
            }
        },
        function (error){
            console.log('Unable to read the directory. Error code: ' +error.code);
        });
        if (level === 0){
            $(document).trigger('endupdate');
        }
        //console.log('Current path analized is ' + path.nativeURL);
    },
    createFilesList: function(idElement, files){
        $('#'+ idElement).empty();
        console.log(files.length);
        if (files == null || files.length == 0){
            $('#'+ idElement).append('<p>No files to show. Would you consider a files update (top right button)?</p>');
            return;
        }
        function getPlayHandler(file){
            return function playHandler(){
                console.log(file.audioName+' '+file.audioPath);
                window.localStorage.setItem("audioForPlay", JSON.stringify(file));
                //$('#musicListButton').hide();
                $('#musicList').collapsible("collapse");
                document.getElementById("selectedTrack").innerHTML=
                    "<a href=\"#\" class=\"ui-collapsible-heading-toggle ui-btn ui-icon-plus ui-btn-inherit\">"+
                    file.audioName+"</a>";
                //$('#selectedTrack').text().innerHTML=file.audioName;
                //$('.class_popCountDown').popup('close');

                //$.mobile.changePage('player.html',{
                //    data:{
                //        file: JSON.stringify(file)
                //    }
                //});
            };
        }
        function getDeleteHandler(file){
            return function deleteHandler(){
                var oldLenght = AppFile.getAppFiles().length;
                var $parentUl = $(this).closest('ul');

                file = new AppFile('', file.fullPath);
                file.deleteFile();
                if (oldLenght === AppFile.getAppFiles().length + 1){
                    $(this).closest('li').remove();
                    $parentUl.listview('refresh');
                } else{
                    console.log('Media not deleted. Something done wrong.');
                    navigator.notification.alert('Media not deleted. Something done wrong so please try again.',
                        function(){},
                        'Error');
                }
            };
        }
        console.log('Create list');
        var $listElement, $linkElement;
        files.sort(AppFile.compareIgnoreCase);
        console.log('test='+files.length);
        for(var i = 0; i < files.length; i++){
            $listElement = $('<li>');
            $linkElement = $('<a>');
            $linkElement
                .attr('href', '#')
                .text(files[i].audioName)
                .click(getPlayHandler(files[i]));

            $listElement.append($linkElement);

            $linkElement = $('<a>');
            $linkElement
                .attr('href', '#')
                .text('Delete')
                .click(getDeleteHandler(files[i]));

            $listElement.append($linkElement);

            $('#' + idElement).append($listElement);
        }
        $('#' + idElement).listview('refresh');
    }
};