/**
 * Created by Dell-STO on 04.03.2015.
 */
document.addEventListener("deviceready", onDeviceReady, false);

_db =window.localStorage;
function onDeviceReady(){
    console.log('onDeviceReady');
    Application.initFilesListPage();


    //console.log(returnSavedCycles());
    _db.setItem("countdownCycles", returnSavedCycles());
    //console.log(window.localStorage.getItem("countdownCycles"));
    //console.log(navigator.vibrate(500));

    //alert("Device is Ready!");
}


function saveCycles(){
    _db.setItem("cyclesCount", document.getElementById("slider").value);
    _db.setItem("countdownCycles", document.getElementById("slider").value);
    _db.setItem("signalType", document.getElementById("signalType").value);
    var cyclesCount = window.localStorage.getItem("cyclesCount");
    location.replace("#settings");
    //alert("data saved = "+cyclesCount);
}

function setCyclesFormElement(){
    var cyclesCount =returnSavedCycles();
    var slider = document.getElementById('slider');
    slider.value = cyclesCount;
    $('.slider').slider('refresh');
    var signalType = returnSignalType();
    if ((signalType == "4") || (signalType == "5")){
        $('#musicListButton').show();
        $('#startPlay').show();
        $('#stopPlay').show();
    }
    else {
        $('#musicListButton').hide();
        $('#startPlay').hide();
        $('#stopPlay').hide();
    }
    console.log("I am ="+document.getElementById("slider").value);
}

function returnSavedCycles(){
    return _db.getItem("cyclesCount");
}

function returnCountdownCycles(){
    return _db.getItem("countdownCycles");
}

function returnSignalType(){
    return _db.getItem("signalType");
}
function returnSelectedAudio(){
    return JSON.parse(_db.getItem("audioForPlay"));
}

function clearLocalStorage(){
    _db.removeItem("audioForPlay");
    _db.removeItem("cyclesCount");
    _db.removeItem("countdownCycles");
    _db.removeItem("signalType");
    _db.removeItem("files");
}

function clearName(){
    _db.removeItem("cyclesCount");
    var slider = document.getElementById('slider');
    slider.value = 3;
    alert("data cleared");
}
