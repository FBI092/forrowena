/**
 * Created by Dell-STO on 25.02.2015.
 */
window.addEventListener("orientationchange", orientationChange, true);

function orientationChange(e) {
    var orientation="portrait";
    if(window.orientation == -90 || window.orientation == 90) {
        orientation = "landscape";
    }
    //document.getElementById("container").innerHTML+=orientation+"<br>";
}

var timer;
var sound;

function countDown(second,typeCountDown) {
    var now = new Date();
    var endMinute=0,endHour=0,endDay=0,endMonth=0,endYear=0;
    second = second || now.getSeconds();
    second = second + now.getSeconds();
    endYear =  endYear || now.getFullYear();
    endMonth = endMonth ? endMonth - 1 : now.getMonth();    //номер месяца начинается с 0
    endDay = endDay || now.getDate();
    endHour = endHour || now.getHours() ;
    endMinute = endMinute || now.getMinutes();

//добавляем секунду к конечной дате (таймер показывает время уже спустя 1с.)
    var endDate = new Date(endYear,endMonth,endDay,endHour,endMinute,second+1);
    var interval = setInterval(function() { //запускаем таймер с интервалом 1 секунду
        var time = endDate.getTime() - now.getTime();
        if (time < 0) {                      //если конечная дата меньше текущей
            clearInterval(interval);
            alert("Incorect date!");
        } else {
            var days = Math.floor(time / 864e5);
            var hours = Math.floor(time / 36e5) % 24;
            var minutes = Math.floor(time / 6e4) % 60;
            var seconds = Math.floor(time / 1e3) % 60;

            //Формат отображение часов
            //шрифт и размер текста
            var digit='<div style="width:70px;float:left;text-align:center">'+
                '<div style="font-family:Stencil;font-size:65px;">';

            // ХЗ еще не разобрался
            var text='</div><div>';

            // значение и текст часов
            if(typeCountDown == 20) {
                var end = '</div></div><div style="float:left;font-size:45px;">:</div>';
                document.getElementById('mytimer').innerHTML = '<div>Last: </div>' +
                digit + minutes + text + '' + end + digit + seconds + text + '';
            }
            else if(typeCountDown == 2)
            {
                var end = '</div></div><div style="float:left;font-size:45px;">:</div>';
                document.getElementById('mytimer2').innerHTML = '<div>Last: </div>' +
                digit + minutes + text + '' + end + digit + seconds + text + '';


            }
            //отображение часов


            if (!seconds && !minutes && !days && !hours) {
                clearInterval(interval);
                if(typeCountDown == 20) {
                        countDown(60,2);
                    //countDown(120,2);
                    generateSignal();
                    location.replace("#active");
                }
                else if(typeCountDown == 2){
                    //alert('it work');
                    countDown(60,20);
                    //countDown(1200,20);
                    window.localStorage.setItem("countdownCycles", (returnCountdownCycles()-1));
                    console.log(window.localStorage.getItem("countdownCycles"));
                    stopSignal();
                    location.replace("#sedentary");
                }
            }
        }
        now.setSeconds(now.getSeconds() + 1); //увеличиваем текущее время на 1 секунду
    }, 1000);
}
//    var timerId=setTimeout(countDown,12000000, 120);

function generateSignal(){
    var x = window.localStorage.getItem("signalType");
    switch (x){
        case '1':
            console.log('Start Beep');
            timer = setTimeout(function run(){
                navigator.notification.beep();
                timer = setTimeout(run, 3000);
            }, 3000);
            break;
        case '2':
            console.log('Start Vibrate');
            timer =setTimeout(function run(){
                console.log(navigator.vibrate(1000));
                timer = setTimeout(run, 3000);
            },3000);
            break;
        case '3':
            console.log('Start Flash');
            window.plugins.flashlight.available(function(isAvailable) {
                console.log(isAvailable);
               if (isAvailable){
                   console.log('isAvailable=true :'+isAvailable);
                   timer = setTimeout(function run(){
                       window.plugins.flashlight.switchOn();
                       setTimeout(window.plugins.flashlight.switchOff(), 1000);
                   }, 2000);

               } else {
                   console.log('isAvailable=false :'+isAvailable);
                   alert('Flashlight not available on this device');
               }
            });

            break;
        case '4':
            console.log('music');
            // console audio after playing
            function audio_win(response)  { console.log('Audio FAILED' + response); }
            function audio_fail(response) { console.log('Audio WINNER' + response); }

// file path for ios
// src = file:///var/mobile/Applications/271BF94B-8540-43FB-944E-39E5AC7A9A3F/[APP-PACKAGE-NAME]/www/sounds/filename.mp3
            var src = location.origin;

// ADDED
// remove file:// if on ios
            if( device.platform === 'iOS' ) {
                src = src.replace('file://', '');
            }

// call the media function
            var audio = new Media(src, audio_win, audio_fail );

// WORKS!
            audio.play();
            break;
        case '5':
            console.log('sound');
            break;
        default :
            console.log('default Vibrate');
            timer = setTimeout(function run(){
                navigator.notification.beep();
                timer = setTimeout(run, 3000);
            }, 3000);
    }
}

function stopSignal(){
    var x = window.localStorage.getItem("signalType");
    switch (x){
        case '1':
            console.log('Stop Beep');
            clearTimeout(timer);
            break;
        case '2':
            console.log('Stop Vibrate');
            clearTimeout(timer);
            break;
        case '3':
            console.log('Stop Flash');
            //clearTimeout(timer);
            break;
        case '4':
            console.log('music2');
            break;
        case '5':
            console.log('sound2');
            break;
        default :
            console.log('Stop default Vibrate');
            clearTimeout(timer);
    }
}

function PlaySound(soundObj) {
    sound = document.getElementById(soundObj);
    if (sound) {
        if (sound.object)
        //IE needs this
            sound.object.Play();
        else
            sound.Play();
    }
}